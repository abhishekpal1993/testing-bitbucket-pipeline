#!/bin/bash

COMMIT_MESSAGE=`git log --format=%B -n 1 $BITBUCKET_COMMIT | cut -d "(" -f1`
echo "Commit type-enum:$COMMIT_MESSAGE"

if [[ ( $COMMIT_MESSAGE == "fix" || $COMMIT_MESSAGE  == "hotfix" ) ]]; then
  echo "Patch release"
  npm version patch --force -m "patch version : %s [skip ci]"
else
  echo "Minor release"
  npm version minor --force -m "minor version : %s [skip ci]"
fi
